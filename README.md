### Automated test bot

This is an automated testbot for ui testing.

### Paths

all the paths and details are in `config.ini` file change them according to tha usecase.

### Usage

Execute the `test_bot.py` it will automatically open link in browser and takes the screenshots and then compares
it with predefined screenhots `ss1.png` and `ss2.png`.

Right now it works for `1920x1080` pixels display with `15.6 inches.` So, `modefy` the `moveTo() points` according to `your system screen`.